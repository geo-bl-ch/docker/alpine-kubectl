FROM alpine:3.15

RUN apk --update add \
        bash \
        curl \
        libc6-compat && \
    mkdir /app && \
    adduser -S -u 1001 -G root -h /app oc && \
    chown -R 1001:0 /app && \
    chmod -R g=u /app && \
    chgrp 0 /etc/passwd && \
    chmod g=u /etc/passwd && \
    curl -LO https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/ocp/4.9.9/openshift-client-linux-4.9.9.tar.gz && \
    tar -xzvf openshift-client-linux-4.9.9.tar.gz && \
    install -o root -g root -m 0755 oc /usr/local/bin/oc

COPY --chown=1001:0 uid_entrypoint.sh /usr/local/bin/

WORKDIR /app

ENTRYPOINT ["/usr/local/bin/uid_entrypoint.sh"]

CMD ["/bin/bash"]

USER 1001
